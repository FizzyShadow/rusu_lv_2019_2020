
def minimal(numbers):
    print('Minimal: ' + str(min(numbers)))
def maximal(numbers):
    print('Maximal: ' + str(max(numbers)))
def average(numbers):
    print('Average: ' + str(sum(numbers)/len(numbers)))

numbers = []
while(1):
    try:
        num = input('Unesite broj: ')
        if(num.isnumeric()):
            numbers.append(float(num))
        elif(num == 'Done'):
            if(numbers):
                minimal(numbers)
                maximal(numbers)
                average(numbers)
            break
        else:
            raise ValueError
         
    except ValueError:
        print('Unesite broj!')