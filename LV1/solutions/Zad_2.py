while(1):
    try:
        ocjena = float(input('Unesite ocjenu: '))
        if( ocjena < 0.0 or ocjena > 1.0):
            raise IndexError
        if(ocjena < 0.6):
            print('F')
        elif(ocjena < 0.7):
            print('D')
        elif(ocjena < 0.8):
            print('C')
        elif(ocjena < 0.9):
            print('B')
        else:
            print('A')
        break
    except ValueError:
        print('Nije unesen broj!')
    except IndexError:
        print('Izvan granica! Unesite ocjenu unutar intervala [0.0, 1.0]')

