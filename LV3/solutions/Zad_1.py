import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


mtcars = pd.read_csv('../resources/mtcars.csv')

#1
print('1. Zadatak\n-----------')
print(mtcars.sort_values('mpg').tail(5))
#2
print('\n2. Zadatak\n-----------')
print(mtcars.sort_values('mpg').head(3).car)
#3
print('\n3. Zadatak\n-----------')
print(sum(mtcars[mtcars.cyl == 6].mpg)/len(mtcars[mtcars.cyl == 6].mpg))
#4
print('\n4. Zadatak\n-----------')
print(np.average(mtcars[(mtcars.wt <= 2.2) & (mtcars.wt >= 2) & (mtcars.cyl == 4)].mpg))
#5
print('\n5. Zadatak\n-----------')
automatic = np.sum(mtcars[mtcars.am == 1].am)
print('Automatic: '+str(automatic) + '\nManual: '+ str(len(mtcars.am)-automatic))
#6
print('\n6. Zadatak\n-----------')
automatichp = np.sum(mtcars[(mtcars.am == 1) & (mtcars.hp > 100)].am)
print('Automatic: '+str(automatichp))
#7
print('\n7. Zadatak\n-----------')
mtcars['wt'] *= 453.59237
print(mtcars)
