import urllib.request
import pandas as pd
import json
import numpy as np
import matplotlib.pyplot as plt

from pandas._libs.tslibs.timestamps import Timestamp

# url that contains valid xml file:
urlA = 'http://iszz.azo.hr/iskzl/rs/podatak/export/json?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2020&vrijemeDo=01.01.2021'

airQualityHR = urllib.request.urlopen(urlA).read()

root = json.loads(airQualityHR)
df = pd.DataFrame(columns=('vrijednost', 'vrijeme'))
i = 0
while True: 
    try:
        root[i]
    except:
        break
    row = dict(zip(['vrijednost', 'vrijeme'], [root[i]['vrijednost'], root[i]['vrijeme']]))
    row_s = pd.Series(row)
    
    row_s.name = i
    df = df.append(row_s)
    df.vrijednost[i] = df.vrijednost[i]
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, unit = 'ms')
df.plot(y='vrijednost', x='vrijeme');

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

print('Zadatak 1\n--------------')
print(df)

#2
print('\nZadatak 2\n--------------')
df = df.sort_values('vrijednost')
print(df.tail(3))

#3
print('\nZadatak 3\n----------------')

noValue = []
mj = np.arange(1,13,1)
for i in range(1,13):
    if(i == 1 or i == 3 or i == 5 or i == 7 or i == 8 or i == 10 or i == 12):
        noValue.append( 31- len(df[df.month == i].vrijednost))
    elif(i == 4 or i == 6 or i == 9 or i == 11):
        noValue.append( 30- len(df[df.month == i].vrijednost))
    else:
        noValue.append( 29- len(df[df.month == i].vrijednost))

plt.figure()
plt.bar(mj, noValue)
plt.show()

#4
print('\nZadatak 4\n----------------')
lipanj = df[df.month == 6].vrijednost
prosinac = df[df.month == 12].vrijednost

plt.figure()
plt.boxplot([lipanj,prosinac])
plt.xticks(np.arange(3), ['', 'Lipanj', 'Prosianc'])
plt.show()

#5
print('\nZadatak 4\n----------------')

radni_dani = df[(df.dayOfweek == 0 )| (df.dayOfweek == 1) | (df.dayOfweek == 2 )| (df.dayOfweek == 3) | (df.dayOfweek == 4)].vrijednost
vikend = df[(df.dayOfweek == 5) | (df.dayOfweek == 6)].vrijednost

plt.figure()
plt.hist([radni_dani, vikend], bins= 20)
plt.legend(['radni dani', 'vikend'])
plt.ylabel('Broj dana')
plt.xlabel('Vrijednost PM10')
plt.show()
