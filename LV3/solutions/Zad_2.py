import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


mtcars = pd.read_csv('../resources/mtcars.csv')
#1
print('Zadatak 1\n--------------')
plt.bar(mtcars[mtcars.cyl == 4].cyl, mtcars[mtcars.cyl == 4].mpg)
plt.bar(mtcars[mtcars.cyl == 6].cyl, mtcars[mtcars.cyl == 6].mpg)
plt.bar(mtcars[mtcars.cyl == 8].cyl, mtcars[mtcars.cyl == 8].mpg)

plt.show()
#2
print('\nZadatak 2\n--------------')
plt.boxplot([mtcars[mtcars.cyl == 4].wt, mtcars[mtcars.cyl == 6].wt, mtcars[mtcars.cyl == 8].wt])
plt.xticks(np.arange(4), ['', '4', '6', '8'])
plt.show()
#3 Automovil sa ručnjim mjenjačem ima manju potrošnju.
print('\nZadatak 3\n--------------')
plt.bar(mtcars[mtcars.am == 0].am, mtcars[mtcars.am == 0].mpg)
plt.bar(mtcars[mtcars.am == 1].am, mtcars[mtcars.am == 1].mpg)
plt.legend(['Automatic', 'Manual'])
plt.grid()
plt.xlim(-2,2)
plt.show()
#4
print('\nZadatak 3\n--------------')
plt.plot(mtcars[mtcars.am == 0].sort_values('hp').hp, mtcars[mtcars.am == 0].sort_values('qsec').qsec)
plt.plot(mtcars[mtcars.am == 1].sort_values('hp').hp, mtcars[mtcars.am == 1].sort_values('qsec').qsec)
plt.xlabel('Hp')
plt.ylabel('Qsec')
plt.legend(['Automatic', 'Manual'])
plt.grid()
plt.show()