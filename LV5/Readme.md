﻿
#RAD NA VJEŽBI

Unutar vježbe se upoznajemo sa algoritmima za grupiranje, te njihovom uporabom. Algoritme koristimo pomoću biblioteke sklearn. Grupirali
smo podatke pomoću algoritma Kmeans koji računa zajedničke centre podacima i algoritam za hijerarhijsko grupiranje. U zadnjem zadatku implementiramo 
kmeans algoritam pomoću navedene literature iz vježbe i predavanja.