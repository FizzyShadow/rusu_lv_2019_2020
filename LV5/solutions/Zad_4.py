from sklearn.cluster import KMeans
from  matplotlib import pyplot as plt
import numpy as np
import matplotlib.image as mpimg

path = '../resources/example_grayscale.png'


img = mpimg.imread(path)
X = img.reshape((-1, 1))
kmeans = KMeans(n_clusters=10)
kmeans.fit(X) 
values = kmeans.cluster_centers_.squeeze()
labels = kmeans.labels_
img_compressed = np.choose(labels, values)
img_compressed.shape = img.shape

plt.figure(1)
plt.imshow(img)
plt.figure(2)
plt.imshow(img_compressed)


#što imamo manje klastera, to se manje centara boja uzima u obzir te je slika više kvantizitana
#što je manji broj clustera to je veća kompresija jer dolazi do manjeg prikaza boja na slici
#kompresija postignuta za 10 clustera je 29.28%