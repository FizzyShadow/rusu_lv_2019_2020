from sklearn.cluster import KMeans
from  matplotlib import pyplot as plt
import numpy as np
import matplotlib.image as mpimg

path = '../resources/example.png'


img = mpimg.imread(path)
X = img.reshape((-1, 1))
kmeans = KMeans(n_clusters=4)
kmeans.fit(X) 
values = kmeans.cluster_centers_.squeeze()
labels = kmeans.labels_
img_compressed = np.choose(labels, values)
img_compressed.shape = img.shape

plt.figure(1)
plt.imshow(img)
plt.figure(2)
plt.imshow(img_compressed)
