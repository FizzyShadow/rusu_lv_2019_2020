from sklearn import datasets
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

def clusterSTD_Exception():
    print("Length of `clusters_std` not consistent with number of centers.")

#1
X = generate_data(500, flagc = 1)
kmeans = KMeans(n_clusters=2, random_state=0).fit_predict(X)
#2
X2 = generate_data(500, flagc = 2)
kmeans2 = KMeans(n_clusters=2, random_state=0).fit_predict(X2)
#3
try:
    X3 = generate_data(500, flagc = 3)
    kmeans3 = KMeans(n_clusters=2, random_state=0).fit_predict(X3)
    plt.figure(4)
    plt.scatter(X3[:, 0], X3[:, 1], c=kmeans3)
except:
     clusterSTD_Exception()
#4
X4 = generate_data(500, flagc = 4)
kmeans4 = KMeans(n_clusters=2, random_state=0).fit_predict(X4)
#5
X5 = generate_data(500, flagc = 5)
kmeans5 = KMeans(n_clusters=2, random_state=0).fit_predict(X5)

plt.figure(1)
plt.plot(X, 'or')
plt.figure(2)
plt.scatter(X[:, 0], X[:, 1], c=kmeans)
plt.figure(3)
plt.scatter(X2[:, 0], X2[:, 1], c=kmeans2)
plt.figure(5)
plt.scatter(X4[:, 0], X4[:, 1], c=kmeans4)
plt.figure(6)
plt.scatter(X5[:, 0], X5[:, 1], c=kmeans5)

#Što više clustera stavimo dolazi do većeg raspoređivanja podataka i obratno.
