import re

path = '../resources/mbox-short.txt'
fopen = open(path, 'r')
emailList = []
firstPartOfEmail = []

for line in fopen:
    line.strip()
    email = re.search('[A-Za-z][\w\.-]+@[\w\.-]+', line)
    if(email):
        email = email.group(0)
        emailList.append(email)
        firstPartOfEmail.append(email[:email.find('@')])

def oneOrMoreA(): # Jedno ili više
    firstPartOfEmailOneOrMoreA = []
    for firstPart in firstPartOfEmail:
        reg = re.match(r'\w+[\.a]+\w+', firstPart)
        if(reg != None):
            reg = reg[0]
            firstPartOfEmailOneOrMoreA.append(reg)
    return firstPartOfEmailOneOrMoreA

def oneA(): #Jedno A
    firstPartOfEmailOneOrMoreA = []
    for firstPart in firstPartOfEmail:
        reg = re.match(r'([b-zB-Z0-9.]*a[b-zB-Z0-9.]*)', firstPart)
        if(reg != None):
            reg = reg[0]
            firstPartOfEmailOneOrMoreA.append(reg)
    return firstPartOfEmailOneOrMoreA

def NoneA(): #Bez A
    firstPartOfEmailOneOrMoreA = []
    for firstPart in firstPartOfEmail:
        reg = re.match(r'[b-zB-Z0-9.]*', firstPart)
        if(reg != None):
            reg = reg[0]
            firstPartOfEmailOneOrMoreA.append(reg)
    return firstPartOfEmailOneOrMoreA

def oneOrMoreNumeric(): #1 ili više num znakova
    firstPartOfEmailOneOrMoreA = []
    for firstPart in firstPartOfEmail:
        reg = re.match(r'\w+\d+\w+', firstPart)
        if(reg != None):
            reg = reg[0]
            firstPartOfEmailOneOrMoreA.append(reg)
    return firstPartOfEmailOneOrMoreA

def lowerCase(): #mala slova
    firstPartOfEmailOneOrMoreA = []
    for firstPart in firstPartOfEmail:
        reg = re.match(r'[a-z]*', firstPart)
        if(reg != None):
            reg = reg[0]
            firstPartOfEmailOneOrMoreA.append(reg)
    return firstPartOfEmailOneOrMoreA
print(oneOrMoreA())
print(oneA())
print(NoneA())
print(oneOrMoreNumeric())
print(lowerCase())