import re

path = '../resources/mbox-short.txt'
fopen = open(path, 'r')
emailList = []
firstPartOfEmail = []

for line in fopen:
    line.strip()
    email = re.search('[A-Za-z][\w\.-]+@[\w\.-]+', line)
    if(email):
        email = email.group(0)
        emailList.append(email)
        firstPartOfEmail.append(email[:email.find('@')])

print(firstPartOfEmail)