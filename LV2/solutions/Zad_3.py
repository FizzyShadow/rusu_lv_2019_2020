import numpy as np
import matplotlib.pyplot as plt

height_m = []
height_z = []

np.random.seed(1)
array01 = np.random.randint(0,2, 100)


for num in array01:
    if(num == 1):
        height_m.append(np.random.normal(180, 7))
    else:
        height_z.append(np.random.normal(167, 7))

plt.style.use('dark_background')
plt.hist([height_m, height_z], color=['blue', 'red'])
plt.title('Raspodjela spolova po visini')


avg_m = np.average(height_m)
avg_z = np.average(height_z)

plt.axvline(avg_m, color='yellow', linestyle='--', linewidth=2)
plt.axvline(avg_z, color='purple', linestyle='--', linewidth=2)
plt.legend(['avg_m', 'avg_z', 'Muskarci', 'Zene'])
plt.show()

print(avg_m,avg_z)