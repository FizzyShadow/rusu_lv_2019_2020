import numpy as np
import matplotlib.pyplot as plt

np.random.seed(2)
bacanje = np.random.randint(1,7,1000)

plt.style.use('dark_background')
plt.grid(linestyle='-')
plt.hist(bacanje, range=[0,7],color=['gray'])
plt.xlabel('Vrijednost bacanja')
plt.ylabel('Broj bacanja')
plt.show()