import numpy as np
import matplotlib.pyplot as plt

avgBacanja=[]
dev=[]
for i in range (0, 1000):                   
    bacanje = np.random.randint(1, 7, size=30)
    avgBacanja.append(np.average(bacanje))
    dev.append(np.std(bacanje))

totalAvg=np.average(avgBacanja)
totalDev=np.std(avgBacanja)

plt.style.use('dark_background')
plt.hist(avgBacanja, color="cyan")
plt.grid()
plt.show()

#primjecuje se gaussova distribucija veliki broj vrijednosti se nalazi pri sredini.