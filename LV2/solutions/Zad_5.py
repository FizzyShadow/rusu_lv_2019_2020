import numpy as np
import matplotlib.pyplot as plt

path = '../resources/mtcars.csv'
fopen = open(path)
cars = []
hp = []
mph = []
weight = []
counter = 0

for line in fopen:
    if(counter == 0):
        counter = counter +1
    else:
        cars.append(line.split(','))

for i in range(len(cars)):
    mph.append(float(cars[i][1]))
    hp.append(int(cars[i][4]))
    weight.append(float(cars[i][6]))

mph.sort()
hp.sort()
weight.sort()


plt.style.use('dark_background')
ax = plt.axes(projection='3d')
plt.plot(mph, hp, weight)
plt.grid()
plt.xlabel('mph')
plt.ylabel('hp')

plt.title('Ovisnost potrošnje automobila o konjskim snagama i težini')
plt.show()
print('Minimal: ' + str(np.min(mph)) + '\nAverage: ' + str(np.average(mph)) + '\nMaximal: ' + str(np.max(mph))+ '\n')