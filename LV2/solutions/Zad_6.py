import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv


path = '../resources/tiger.png'
image = cv.imread(path)
hsv=cv.cvtColor(image, cv.COLOR_BGR2HSV)

h, s, v = cv.split(hsv)
v += 50
final_hsv = cv.merge((h, s, v))
RGB_img=cv.cvtColor(final_hsv, cv.COLOR_HSV2RGB)
cv.imshow('image',RGB_img)
cv.waitKey(0)


