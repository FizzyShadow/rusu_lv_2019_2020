import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sklearn.linear_model as lm
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.datasets import load_boston

boston_data = load_boston()
boston = pd.DataFrame(boston_data.data, columns=boston_data.feature_names)
boston['MEDV'] = boston_data.target

X = pd.DataFrame(np.c_[boston['LSTAT']], columns = ['LSTAT'])
Y = boston['MEDV']
    
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.3, random_state=5)

linearModel = lm.LinearRegression()
linearModel.fit(X_train,Y_train)

ytest_p = linearModel.predict(X_test)
MSE_test = mean_squared_error(Y_test, ytest_p)

plt.figure(1)
plt.plot(X_test,ytest_p,'og',label='predicted')
plt.plot(X_test,Y_test,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,40])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)
plt.xlabel('LSTAT')
plt.ylabel('MEDV')