import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
 
x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

#degree  LinearRegresion= 15
MSE_test=[]
poly15 = PolynomialFeatures(degree=15)
xnew15 = poly15.fit_transform(x)

np.random.seed(12)
indeksi = np.random.permutation(len(xnew15))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew15)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew15)))+1:len(xnew15)]

xtrain = xnew15[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnew15[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModel15 = lm.LinearRegression()
linearModel15.fit(xtrain,ytrain)

ytest_p = linearModel15.predict(xtest)
MSE_test.append(mean_squared_error(ytest, ytest_p))

plt.figure(2)
plt.plot(xtest[:,1],ytest_p,'og',label='predicted')
plt.plot(xtest[:,1],ytest,'or',label='test')
plt.legend(loc = 4)

#degree ridge regresion = 15
polyRidge = PolynomialFeatures(degree=15)
xnewRidge = polyRidge.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnewRidge))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnewRidge)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnewRidge)))+1:len(xnewRidge)]

xtrain = xnewRidge[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnewRidge[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModelRidge = lm.Ridge()   
linearModelRidge.fit(xtrain,ytrain)

ytest_p = linearModelRidge.predict(xtest)
MSE_test.append(mean_squared_error(ytest, ytest_p))

polyLasso = PolynomialFeatures(degree=15)
xnewLasso = polyLasso.fit_transform(x)
    
np.random.seed(12)
indeksi = np.random.permutation(len(xnewLasso))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnewLasso)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnewLasso)))+1:len(xnewLasso)]

xtrain = xnewLasso[indeksi_train,]
ytrain = y_measured[indeksi_train]

xtest = xnewLasso[indeksi_test,]
ytest = y_measured[indeksi_test]

linearModelLasso = lm.Lasso()
linearModelLasso.fit(xtrain,ytrain)

ytest_p = linearModelLasso.predict(xtest)
MSE_test.append(mean_squared_error(ytest, ytest_p))

#ZADATAK 8

print('Model Poly regresije je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel15.intercept_, '+', linearModel15.coef_, '*x')
print('Model Ridge regresije je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModelRidge.intercept_, '+', linearModelRidge.coef_, '*x')
print('Model Lasso regresije je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModelLasso.intercept_, '+', linearModelLasso.coef_, '*x')

plt.figure(2)
plt.plot(x,y_true,label='f')
plt.plot(x, linearModel15.predict(xnewRidge),'y-',label='Ridge model=15')
plt.plot(x, linearModel15.predict(xnew15),'r-',label='Linear model=15')
plt.plot(x, linearModel15.predict(xnewLasso),'g-',label='Lasso model=15')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1],ytrain,'ok',label='train')
plt.legend(loc = 4)

#Koficijent kod ridge regresije ima puno bolje raspoređene vrijednosti nego kod poly regresije, a kod Lasso regresije se vrijednosti raspoređuju na jako male vrijednosti.


