import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

def pravac(x, theta):

    return theta[1]*x + theta[0]

def kriterijJ(x,y,theta):
    J = 0.0
    n = x.shape[0]
    for i in range(0,n):
        J += (pravac(x[i],theta) - y[i]) ** 2
    
    J /= (2*n)

    return J

def direct_theta(n_samples, xtrain, ytrain):
    arrayx = np.ones((n_samples, 1))
    arrayx = np.append(arrayx, xtrain, axis=1)
    return np.linalg.inv(np.transpose(arrayx)@arrayx)@np.transpose(arrayx)@ytrain

def direct_theta(n_samples, xtrain, ytrain):
    arrayx = np.ones((n_samples, 1))
    arrayx = np.append(arrayx, xtrain, axis=1)
    return np.linalg.inv(np.transpose(arrayx)@arrayx)@np.transpose(arrayx)@ytrain

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)


#ZADATAK 3
n_samples = 70
dir_theta = direct_theta(n_samples,xtrain,ytrain)
xp = np.array([x.min(), x.max()])
yp = np.array([pravac(xp[0],dir_theta), pravac(xp[1],dir_theta)])
no_iter = 50
theta_old = np.zeros((2,1))
theta_new = np.zeros((2,1))
dulj_koraka = 0.01
J = np.zeros((no_iter,1))
x = xtrain
y = ytrain
plt.figure(4)
plt.scatter(x, y, marker='.')   
plt.title("gradijentna metoda")

for iter in range(0, no_iter):

        J[iter] = kriterijJ(x,y,theta_old)
        
        rj0 = 0.0
        rj1 = 0.0

        for i in range(0,n_samples):
            rj0 += pravac(x[i],theta_old) - y[i]
            rj1 += (pravac(x[i],theta_old) - y[i])*x[i]

        rj0 /= n_samples
        rj1 /= n_samples

        theta_new[0] = theta_old[0] - dulj_koraka * rj0
        theta_new[1] = theta_old[1] - dulj_koraka * rj1
        theta_old = theta_new

        yp[0] = pravac(xp[0],theta_new)
        yp[1] = pravac(xp[1],theta_new)
        
        plt.plot(xp,yp)
        
    

  
print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', theta_new[0], '+', theta_new[1], '*x')      
#Što je veći korak to dolazi do većeg odstupanja od poželjnog tj. što je korak manji to se bolje vidi učenje iako 
#korak ne treba biti pre mali jer nebi dobili željene rezultate.
#Gradijentna metoda puno bolje određuje parametre modela uz dobro odabrani korak










